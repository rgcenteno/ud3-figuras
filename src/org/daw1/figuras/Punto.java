/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.figuras;

import java.lang.Math;
/**
 * Representa un punto en el plano x, y
 * @author Rafael González Centeno
 */
public class Punto {
    final private int x;
    private int y;

    public Punto(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Devuelve la posición x
     * @return posición x
     */
    public int getX() {
        return x;
    }

    /**
     * Devuelve la posición y
     * @return posición y
     */
    public int getY() {
        return y;
    }

    /**
     * Calcula la distancia entre dos puntos
     * @param p Punto destino
     * @return Distancia entre ambos puntos
     */
    public double getDistancia(Punto p){
        return Math.sqrt(Math.pow(this.x - p.getX(), 2) + Math.pow(this.y - p.getY(), 2));
    }
}
