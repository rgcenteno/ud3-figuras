/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.figuras;

/**
 * Representa un triángulo rectángulo en base a sus parámetros base y altura
 * @author Rafael González Centeno
 */
public class TrianguloRectangulo implements Figura{
    final private int base;
    final private int altura;

    /**
     * Número de lados de un triángulo rectángulo
     */
    final public static int NUM_LADOS = 3;
    
    /**
     * Crea un triángulo rectángulo con las bases y alturas dadas
     * @param base Base del triángulo. Valor &gt; 0.
     * @param altura Altura del triángulo. Valor &gt; 0;
     * @throws IllegalArgumentException si la base o la altura es menor o igual que cero
     */
    public TrianguloRectangulo(int base, int altura) {
        checkBase(base);
        checkAltura(altura);
        this.base = base;
        this.altura = altura;
    }
    
    private static void checkBase(int medida){
        if(medida <= 0){
            throw new IllegalArgumentException("No se aceptan valores <= 0 para la base");
        }
    }
    
    private static void checkAltura(int medida){
        if(medida <= 0){
            throw new IllegalArgumentException("No se aceptan valores <= 0 para la altura");
        }
    }
    
    /**
     * Obtiene el área del triángulo
     * @return área del triángulo
     */
    @Override
    public double getArea(){
        return (base * altura) / 2;
    }
    
    /**
     * Obtiene el perímetro del triángulo
     * @return perímetro del triángulo 
     */
    @Override
    public double getPerimetro(){
        double hipotenusa = Math.sqrt(Math.pow(base, 2) + Math.pow(altura, 2));
        return base + altura + hipotenusa;
    }
    
}
