/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.figuras;

/**
 * Define los métodos que debe implementar una clase figura
 * @author Rafael González Centeno
 */
public interface Figura {
    /**
     * Obtiene el área de la figura
     * @return área de la figura
     */
    public double getArea();
    /**
     * Obtiene el perímetro de la figura
     * @return perímetro de la figura
     */
    public double getPerimetro();
}
