/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.figuras;

/**
 * Representa un rectángulo en base a sus parámetros base y altura
 * @author Rafael González Centeno
 */
public class Rectangulo implements Figura{
    final private int base;
    final private int altura;
    
    /**
     * Número de lados de un rectángulo
     */    
    final public static int NUM_LADOS = 4;

    /**
     * Crea un rectángulo con la base y altura especificadas
     * @param base Base del rectángulo. Valor mayor que cero.
     * @param altura Altura del rectángulo. Valor mayor que cero.
     * @throws IllegalArgumentException si el la base o altura es menor o igual que cero
     */
    public Rectangulo(int base, int altura) {
        checkBase(base);
        checkAltura(altura);
        this.base = base;
        this.altura = altura;
    }
    
    private static void checkBase(int medida){
        if(medida <= 0){
            throw new IllegalArgumentException("No se aceptan valores <= 0 para la base");
        }
    }
    
    private static void checkAltura(int medida){
        if(medida <= 0){
            throw new IllegalArgumentException("No se aceptan valores <= 0 para la altura");
        }
    }
    
    /**
     * Obtiene el área del rectángulo
     * @return área del rectángulo
     */
    @Override
    public double getArea(){ 
       return (base * altura);
    }
    
    /**
     * Obtiene el perímetro del rectángulo
     * @return perímetro del rectángulo
     */
    @Override
    public double getPerimetro(){
        return 2 * base + 2 * altura;
    }
}
