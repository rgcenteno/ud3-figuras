/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.figuras;

/**
 * Representa un cuadrado en base a su lado
 * @author Rafael González Centeno
 */
public class Cuadrado extends Rectangulo {
    
    /**
     * Crea un cuadrado con el lado pasado como parámetro
     * @param lado del triángulo
     * @throws IllegalArgumentException si el lado es menor o igual que cero
     */    
    public Cuadrado(int lado) {
        super(lado, lado);
    }
    
}
