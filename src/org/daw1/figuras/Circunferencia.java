/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.figuras;

/**
 * Representa una circunferencia en base a su radio
 * @author Rafael González Centeno
 */
public class Circunferencia implements Figura{
    final private int radio;
    
    /**
     * Número de lados de la circunferencia
     */
    final public static int NUM_LADOS = 0;

    /**
     * Crea una circunferencia de un radio dado
     * @param radio Radio de la circunferencia. Debe ser mayor que cero.
     * @throws IllegalArgumentException si el radio es menor o igual que cero
     */
    public Circunferencia(int radio) {
        checkRadio(radio);
        this.radio = radio;
    }        
    
    private static void checkRadio(int radio){
        if(radio < 0){
            throw new IllegalArgumentException("El radio debe ser mayor que cero");
        }
    }
    
    /**
     * Obtiene el área de la circunferencia
     * @return Área de la circunferencia
     */
    @Override
    public double getArea(){
        return Math.PI * Math.pow(radio, 2);
    }
    
    /**
     * Obtiene el perímetro de la circunferencia
     * @return perímetro de la circunferencia
     */
    @Override
    public double getPerimetro(){
        return 2 * Math.PI * radio;
    }
    
}
